"""Utility functions for exection"""

import subprocess
from datetime import datetime
from os.path import join
from typing import Any, Mapping, Tuple

from data_wrangling.load_data import DataLoader, PairedDataLoader
from models.pix2pix import build_pix2pix
from models.adv_pix2pix import build_dense_pix2pix


def get_model(
        model_name: str,
        input_img_dim: Tuple[int, int, int],
        **kwargs):
    """Return Keras model based on model name string"""
    if model_name == 'Pix2Pix':
        vs_model = build_pix2pix(input_img_dim=input_img_dim, **kwargs)
    elif model_name == 'DensePix2Pix':
        vs_model = build_dense_pix2pix(input_img_dim=input_img_dim, **kwargs)
    else:
        raise ValueError(f'{model_name} not found!')
    return vs_model

def get_data_loader(config: Mapping[str, Any], dataset_type: str='train'):
    """Return data loader based on the experiment type"""
    if config.get('data_loader_type', None) == 'unpaired':
        data_loader = DataLoader(config=config)
    else:
        data_loader = PairedDataLoader(
            config=config, dataset_type=dataset_type)
    return data_loader

def add_commit_hash_to_readme(readme_path: str, msg: str = 'logging') -> None:
    """Appends current commit hash to the readme file with timestamp"""
    date = str(datetime.now().date().strftime("%d-%m-%Y"))
    time = str(datetime.now().time().strftime("%H:%M:%S"))
    commit_hash = subprocess.check_output(['git', 'rev-parse', 'HEAD']).decode('ascii').strip()
    with open(join(readme_path, 'readme.txt'), 'a') as readme_file:
        readme_file.write("\n")
        readme_file.write(f'{date} {time}: {msg} | commit hash: {commit_hash}')