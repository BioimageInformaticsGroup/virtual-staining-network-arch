"""Implement violin plotting for different evaluation metric"""

from argparse import ArgumentParser
from collections import defaultdict
from os import listdir, makedirs
from os.path import join

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from tqdm import tqdm



def plot_comparison_violin(
        bl_dir: str,
        dc_dir: str,
        dns_dir: str,
        output_dir: str,
        only_tissue_tiles: bool
    ):
    """Plot comparison violins of two trainings"""
    bl_pkl_list = [pkl for pkl in listdir(bl_dir) if '.pkl' in pkl]
    dc_pkl_list = [pkl for pkl in listdir(dc_dir) if '.pkl' in pkl]
    dns_pkl_list = [pkl for pkl in listdir(dns_dir) if '.pkl' in pkl]

    bl_pkl_list.sort()
    dc_pkl_list.sort()
    dns_pkl_list.sort()
    #for main_pkl, sec_pkl in tqdm(zip(main_pkl_list, sec_pkl_list)):
    #    print(main_pkl, sec_pkl)
    #exit(0)
    bl_eval_dict = defaultdict(list)
    dc_eval_dict = defaultdict(list)
    dns_eval_dict = defaultdict(list)
    # Sample-wise evaluation
    #ranges = {'simm': (0, 1), "psnr": (0, 35), "prcr": (0, 1)}
    labels = ['' for _ in np.arange(0, 15)]
    labels[3] = 'Baseline'
    labels[7] = 'Double conv.'
    labels[11] = 'Dense conv.'

    for bl_pkl, dc_pkl, dns_pkl in tqdm(zip(bl_pkl_list, dc_pkl_list, dns_pkl_list)):
        bl_pkl_path = join(bl_dir, bl_pkl)
        dc_pkl_path = join(dc_dir, dc_pkl)
        dns_pkl_path = join(dns_dir, dns_pkl)

        bl_df = pd.read_pickle(bl_pkl_path)
        dc_df = pd.read_pickle(dc_pkl_path)
        dns_df = pd.read_pickle(dns_pkl_path)
        for eval_metric in ['simm', 'psnr', 'prcr']:
            if only_tissue_tiles:
                bl_arr = list(bl_df.loc[bl_df['full_tissue'] == True][eval_metric])
                dc_arr = list(dc_df.loc[dc_df['full_tissue'] == True][eval_metric])
                dns_arr = list(dns_df.loc[dns_df['full_tissue'] == True][eval_metric])
            else:
                bl_arr = list(bl_df[eval_metric])
                dc_arr = list(dc_df[eval_metric])
                dns_arr = list(dns_df[eval_metric])
            bl_eval_dict[eval_metric].extend(bl_arr)
            dc_eval_dict[eval_metric].extend(dc_arr)
            dns_eval_dict[eval_metric].extend(dns_arr)
            makedirs(output_dir, exist_ok=True)

    plt.rcParams.update({'font.size': 6})
    # Overall evaluation
    for eval_metric in ['simm', 'psnr', 'prcr']:
        _, axes = plt.subplots(figsize=(3, 2), dpi=300)
        for axis in ['top', 'bottom', 'left', 'right']:
            axes.spines[axis].set_linewidth(0.5)  # change width
            axes.spines[axis].set_color('gray')
        axes.xaxis.set_tick_params(width=0.5)
        axes.yaxis.set_tick_params(width=0.5)

        #title = 'PCC' if eval_metric == 'prcr' else eval_metric.upper()
        #plt.title(title)
        data = np.vstack(
            (bl_eval_dict[eval_metric], dc_eval_dict[eval_metric], dns_eval_dict[eval_metric])
        ).T
        vln_plot = axes.violinplot(
            data, [3, 7, 11], widths=3, showmeans=False, showmedians=False, showextrema=False)
        for body, clr in zip(vln_plot['bodies'], ['blue', 'red', 'green']):
            body.set_facecolor(clr)
            body.set_alpha(0.5)

        axes.hlines(np.min(bl_eval_dict[eval_metric]), 2.75, 3.25, linewidth=0.5)
        axes.hlines(np.max(bl_eval_dict[eval_metric]), 2.75, 3.25, linewidth=0.5)
        axes.hlines(np.mean(bl_eval_dict[eval_metric]), 2.75, 3.25, linewidth=0.5)
        axes.hlines(
            np.percentile(bl_eval_dict[eval_metric], 25),
            2.75, 3.25, linestyles='dotted', linewidth=0.5)
        axes.hlines(
            np.percentile(bl_eval_dict[eval_metric], 75),
            2.75, 3.25, linestyles='dotted', linewidth=0.5)
        #ax.text(1.75, np.min(bl_eval_dict[eval_metric]), 'min', va='center')
        #ax.text(1.75, np.max(bl_eval_dict[eval_metric]), 'max', va='center')
        #ax.text(1.5, np.mean(bl_eval_dict[eval_metric]), 'mean', va='center')
        #ax.text(1.75, np.percentile(bl_eval_dict[eval_metric], 25), 'q25', va='center')
        #ax.text(1.75, np.percentile(bl_eval_dict[eval_metric], 75), 'q75', va='center')

        axes.hlines(np.min(dc_eval_dict[eval_metric]), 6.75, 7.25, linewidth=0.5)
        axes.hlines(np.max(dc_eval_dict[eval_metric]), 6.75, 7.25, linewidth=0.5)
        axes.hlines(np.mean(dc_eval_dict[eval_metric]), 6.75, 7.25, linewidth=0.5)
        axes.hlines(
            np.percentile(dc_eval_dict[eval_metric], 25),
            6.75, 7.25, linestyles='dotted', linewidth=0.5)
        axes.hlines(
            np.percentile(dc_eval_dict[eval_metric], 75),
            6.75, 7.25, linestyles='dotted', linewidth=0.5)
        #ax.text(5.75, np.min(dc_eval_dict[eval_metric]), 'min', va='center')
        #ax.text(5.75, np.max(dc_eval_dict[eval_metric]), 'max', va='center')
        #ax.text(5.5, np.mean(dc_eval_dict[eval_metric]), 'mean', va='center')
        #ax.text(5.75, np.percentile(dc_eval_dict[eval_metric], 25), 'q25', va='center')
        #ax.text(5.75, np.percentile(dc_eval_dict[eval_metric], 75), 'q75', va='center')

        axes.hlines(np.min(dns_eval_dict[eval_metric]), 10.75, 11.25, linewidth=0.5)
        axes.hlines(np.max(dns_eval_dict[eval_metric]), 10.75, 11.25, linewidth=0.5)
        axes.hlines(np.mean(dns_eval_dict[eval_metric]), 10.75, 11.25, linewidth=0.5)
        axes.hlines(
            np.percentile(dns_eval_dict[eval_metric], 25),
            10.75, 11.25, linestyles='dotted', linewidth=0.5)
        axes.hlines(
            np.percentile(dns_eval_dict[eval_metric], 75),
            10.75, 11.25, linestyles='dotted', linewidth=0.5)
        #ax.text(9.75, np.min(dns_eval_dict[eval_metric]), 'min', va='center')
        #ax.text(9.75, np.max(dns_eval_dict[eval_metric]), 'max', va='center')
        #ax.text(9.5, np.mean(dns_eval_dict[eval_metric]), 'mean', va='center')
        #ax.text(9.75, np.percentile(dns_eval_dict[eval_metric], 25), 'q25', va='center')
        #ax.text(9.75, np.percentile(dns_eval_dict[eval_metric], 75), 'q75', va='center')

        set_axis_style(axes, labels)
        #ax.set(xlim=(0, 9), xticks=np.arange(1, 9),
        #    ylim=(0, 100), yticks=np.arange(1, 100))
        plt.grid(True, linewidth=0.25)
        plt.savefig(join(output_dir, f'violin_{eval_metric}_overall.png'))
        plt.close()
        #plt.show()

def set_axis_style(axs, labels):
    "Set axis style as per number of distributions to be compared"
    axs.xaxis.set_tick_params(direction='out')
    axs.xaxis.set_ticks_position('bottom')
    axs.set_xticks(np.arange(0, len(labels)))#, labels=labels)
    axs.set_xticklabels(labels)

    #y_label = ['' for _ in np.arange(0, 8)]
    #axs.yaxis.set_tick_params(direction='out')
    #axs.yaxis.set_ticks_position('left')
    #axs.set_yticks(np.arange(0, len(y_label)))

    #ax.set_xlim(0.25, len(labels) + 0.75)
    #axs.set_xlabel('Pix2pix Variants')

def main():
    """Takes aguements and initiate violin plotting"""

    parser = ArgumentParser()
    parser.add_argument(
        '--bl_pkl_dir', help='Main training pickles directory',
        type=str, required=True)
    parser.add_argument(
        '--dc_pkl_dir', help='Secondary training pickles directory',
        type=str, required=True
    )
    parser.add_argument(
        '--dns_pkl_dir', help='Secondary training pickles directory',
        type=str, required=True
    )
    parser.add_argument(
        '--output_dir', help='Output directory for comparison violins',
        type=str, required=True
    )
    parser.add_argument('--only_tissue_tiles', action='store_true')

    param = parser.parse_args()
    print(param.only_tissue_tiles)
    plot_comparison_violin(
        bl_dir=param.bl_pkl_dir,
        dc_dir=param.dc_pkl_dir,
        dns_dir=param.dns_pkl_dir,
        output_dir=param.output_dir,
        only_tissue_tiles=param.only_tissue_tiles
    )

if __name__ == '__main__':
    main()
