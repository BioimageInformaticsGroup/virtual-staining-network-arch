"""SIMM Score overlay"""

from argparse import ArgumentParser
from os import listdir
from os.path import join

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from PIL import Image
from tqdm import tqdm

FULL_RES = {
    "sample_18": (37667, 35477),
    "sample_25": (34903, 33621),
    "sample_38": (40432, 31747),
    "sample_42": (39051, 33625),
    "sample_46": (40449, 39213),
    "sample_13": (40462, 31772),
    "sample_54": (23773, 33606),
    "sample_61": (26570, 35451),
    "sample_80": (27942, 37325),
    "sample_87": (26551, 37318),
    "sample_93": (27933, 37296),
    "sample_96": (26533, 37295),
    "sample_3": (29326, 29907)
}

def plot_sample_heatmap_overlay(
        sample_dir: str,
        pickle_dir: str,
        output_dir: str
    ):
    """Plot heatmap overlays using sample tilewise SIMM score"""
    imgs = [img for img in listdir(sample_dir) if '.jpg' in img]
    pkls = [pkl for pkl in listdir(pickle_dir) if '.pkl' in pkl]

    imgs.sort()
    pkls.sort()

    for img, pkl in tqdm(zip(imgs, pkls)):
        sample_num = pkl.split('_')[1]
        dims = FULL_RES[f'sample_{sample_num}']
        df_pkl = pd.read_pickle(join(pickle_dir, pkl))

        full_res_heatmap = np.zeros(dims)
        #full_res_mask = np.zeros(dims, dtype=int)
        tile_size = 2048
        stride = 2048
        rows = np.floor((dims[0] - tile_size + stride) / stride).astype("int")
        cols = np.floor((dims[1] - tile_size + stride) / stride).astype("int")

        for i in range(rows):
            for j in range(cols):
                h_start = i * stride
                w_start = j * stride
                tile = f'sample_{sample_num}_unstained_{i}_{j}'
                tile_scores = df_pkl.loc[df_pkl['tile'] == tile]

                if len(list(tile_scores['simm'])) == 0:
                    continue
                full_res_heatmap[h_start : h_start + tile_size,\
                    w_start : w_start + tile_size] = list(tile_scores['simm'])[0]
                #full_res_mask[h_start : h_start + tile_size,\
                #    w_start : w_start + tile_size] = list(tile_scores['full_tissue'])[0]

        low_res_heatmap = full_res_heatmap[::10, ::10]
        #low_res_mask = full_res_mask[::10, ::10]

        pil_img = Image.open(join(sample_dir, img))
        np_img = np.asarray(pil_img).astype('uint8')
        #masked_img = np_img.astype('uint8')
        #masked_img[low_res_mask < 1] = 255
        #low_res_heatmap[low_res_mask < 1] = 0

        del full_res_heatmap
        #del full_res_mask

        #Plot image and overlay colormap
        _, axi = plt.subplots(1, 1)
        #axi[0].imshow(np_img) masked_img
        #axi[0].axis('off')
        axi.imshow(np_img) 
        shw2  = axi.imshow(low_res_heatmap, interpolation = 'nearest', alpha=0.6, cmap='RdYlGn')
        _ = plt.colorbar(shw2)
        plt.savefig(join(output_dir, f'sample_{sample_num}_heatmap.png'))
        #plt.show()
        #exit(0)

def main():
    """Takes aguements and initiate histogram plotting"""

    parser = ArgumentParser()
    parser.add_argument(
        '--sample_dir', help='Downsampled sample images directory',
        type=str, required=True)
    parser.add_argument(
        '--pickle_dir', help='Tilewise stats pickled df directory',
        type=str, required=True
    )
    parser.add_argument(
        '--output_dir', help='Sample SIMM heatmap overlay output directory',
        type=str, required=True
    )
    #parser.add_argument('--only_tissue_tiles', action='store_true')

    param = parser.parse_args()

    plot_sample_heatmap_overlay(
        sample_dir=param.sample_dir,
        pickle_dir=param.pickle_dir,
        output_dir=param.output_dir
    )

if __name__ == '__main__':
    main()
