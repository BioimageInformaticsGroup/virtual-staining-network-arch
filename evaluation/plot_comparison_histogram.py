"""Implement histogram plotting for different evaluation metric"""

from argparse import ArgumentParser
from collections import defaultdict
from os import listdir#, makedirs
from os.path import join

import pandas as pd
from matplotlib import pyplot as plt
from tqdm import tqdm
import numpy as np

def plot_comparison_histogram(
        bl_dir: str,
        dc_dir: str,
        dns_dir: str,
        output_dir: str,
        only_tissue_tiles: bool
    ):
    """Plot comparison histograms of two trainings"""
    bl_pkl_list = [pkl for pkl in listdir(bl_dir) if '.pkl' in pkl]
    dc_pkl_list = [pkl for pkl in listdir(dc_dir) if '.pkl' in pkl]
    dns_pkl_list = [pkl for pkl in listdir(dns_dir) if '.pkl' in pkl]
    bl_pkl_list.sort()
    dc_pkl_list.sort()
    dns_pkl_list.sort()

    #for main_pkl, sec_pkl in tqdm(zip(main_pkl_list, sec_pkl_list)):
    #    print(main_pkl, sec_pkl)
    #exit(0)
    bl_eval_dict = defaultdict(list)
    dc_eval_dict = defaultdict(list)
    dns_eval_dict = defaultdict(list)
    # Sample-wise evaluation
    #mleg = 'Dense conv.'
    #sleg = 'Baseline'
    #ranges = {'simm': (0, 1), "psnr": (0, 35), "prcr": (0, 1)}
    for bl_pkl, dc_pkl, dns_pkl in tqdm(zip(bl_pkl_list, dc_pkl_list, dns_pkl_list)):
        #sample_name = f'sample_{bl_pkl.split("_")[1]}'
        bl_pkl_path = join(bl_dir, bl_pkl)
        dc_pkl_path = join(dc_dir, dc_pkl)
        dns_pkl_path = join(dns_dir, dns_pkl)
        bl_df = pd.read_pickle(bl_pkl_path)
        dc_df = pd.read_pickle(dc_pkl_path)
        dns_df = pd.read_pickle(dns_pkl_path)
        for eval_metric in ['simm', 'psnr', 'prcr']:
            if only_tissue_tiles:
                bl_arr = list(bl_df.loc[bl_df['full_tissue'] == True][eval_metric])
                dc_arr = list(dc_df.loc[dc_df['full_tissue'] == True][eval_metric])
                dns_arr = list(dns_df.loc[dns_df['full_tissue'] == True][eval_metric])
            else:
                bl_arr = list(bl_df[eval_metric])
                dc_arr = list(dc_df[eval_metric])
                dns_arr = list(dns_df[eval_metric])
            bl_eval_dict[eval_metric].extend(bl_arr)
            dc_eval_dict[eval_metric].extend(dc_arr)
            dns_eval_dict[eval_metric].extend(dns_arr)
            # plt.title(f'{sample_name}')
            # plt.hist(
            #     bl_arr, density=False, bins=100, range=ranges[eval_metric],
            #     fc='green', alpha=0.5, histtype='bar', label=mleg)
            # plt.hist(dc_arr, density=False, bins=100, range=ranges[eval_metric],
            #     fc='blue', alpha=0.5, histtype='bar', label=sleg)
            # plt.ylabel('Tiles')
            # plt.xlabel(f'{eval_metric} Score')
            # plt.grid(True)
            # plt.legend()
            # makedirs(output_dir, exist_ok=True)
            # plt.savefig(join(output_dir, f'{eval_metric}_{sample_name}.png'))
            # plt.close()

    # Overall evaluation
    for eval_metric in ['simm', 'psnr', 'prcr']:
        title = 'PCC' if eval_metric == 'prcr' else eval_metric.upper()
        plt.title(title)
        # plt.hist(
        #     bl_eval_dict[eval_metric], density=False, range=ranges[eval_metric],
        #     fc='green', bins=100, alpha=0.5, histtype='bar', label=mleg)
        # plt.hist(
        #     dc_eval_dict[eval_metric], density=False, range=ranges[eval_metric],
        #     fc='blue', bins=100, alpha=0.5, histtype='bar', label=sleg)
        bl_hist, bl_bins = np.histogram(bl_eval_dict[eval_metric], bins=10)
        dc_hist, dc_bins = np.histogram(dc_eval_dict[eval_metric], bins=10)
        dns_hist, dns_bins = np.histogram(dns_eval_dict[eval_metric], bins=10)
        bl_bins = list(bl_bins)
        dc_bins = list(dc_bins)
        dns_bins = list(dns_bins)
        bl_bins_avg = [(bl_bins[i]+bl_bins[i+1])/2 for i in range(len(bl_bins) - 1)]
        dc_bins_avg = [(dc_bins[i]+dc_bins[i+1])/2 for i in range(len(dc_bins) - 1)]
        dns_bins_avg = [(dns_bins[i]+dns_bins[i+1])/2 for i in range(len(dns_bins) - 1)]
        #dc_hist, dc_bins = smoother(dc_hist, dc_bins[:-1])
        #dns_hist, dns_bins = smoother(dns_hist, dns_bins[:-1])
        plt.rcParams.update({'font.size': 6})
        _, axes = plt.subplots(figsize=(3, 2), dpi=300)
        for axis in ['top', 'bottom', 'left', 'right']:
            axes.spines[axis].set_linewidth(0.5)  # change width
            axes.spines[axis].set_color('gray')
        axes.xaxis.set_tick_params(width=0.5)
        axes.yaxis.set_tick_params(width=0.5)
        plt.plot(bl_bins_avg, bl_hist, color='blue', alpha=0.5, label='Baseline')
        plt.plot(dc_bins_avg, dc_hist, color='red', alpha=0.5, label='Double conv.')
        plt.plot(dns_bins_avg, dns_hist, color='green', alpha=0.5, label='Dense conv.')

        #plt.ylabel('Tiles')
        #plt.xlabel(f'{eval_metric} Score')
        plt.grid(True, linewidth=0.25)
        plt.legend()
        #plt.show()
        plt.savefig(join(output_dir, f'density_{eval_metric}_unsmth.png'))
        plt.close()

# def smoother(arr, bins):
#     from scipy.interpolate import make_interp_spline
#     xnew = np.linspace(bins.min(), bins.max(), 100)
#     spl = make_interp_spline(bins, arr, k=2)
#     power_smooth = spl(xnew)
#     return power_smooth, xnew

def main():
    """Takes aguements and initiate histogram plotting"""

    parser = ArgumentParser()
    parser.add_argument(
        '--bl_pkl_dir', help='Baseline training pickles directory',
        type=str, required=True)
    parser.add_argument(
        '--dc_pkl_dir', help='Double conv. training pickles directory',
        type=str, required=True
    )
    parser.add_argument(
        '--dns_pkl_dir', help='Double conv. training pickles directory',
        type=str, required=True
    )
    parser.add_argument(
        '--output_dir', help='Output directory for comparison histograms',
        type=str, required=True
    )
    parser.add_argument('--only_tissue_tiles', action='store_true')

    param = parser.parse_args()
    print(param.only_tissue_tiles)
    plot_comparison_histogram(
        bl_dir=param.bl_pkl_dir,
        dc_dir=param.dc_pkl_dir,
        dns_dir=param.dns_pkl_dir,
        output_dir=param.output_dir,
        only_tissue_tiles=param.only_tissue_tiles
    )

if __name__ == '__main__':
    main()
