"""Tile wise evaluation of WSI"""

from collections import defaultdict
from itertools import product
from multiprocessing import Pool
from os import listdir
from os.path import basename, join
from time import perf_counter
from typing import Any, Dict, List, Mapping, Optional, Sequence, Tuple

import attr
import pandas as pd
from numpy import array, dot
from numpy import max as np_max
from numpy import mean as np_mean
from numpy import min as np_min
from numpy import ndarray, percentile
from PIL import Image
from scipy.stats import pearsonr
from skimage.metrics import peak_signal_noise_ratio as psnr
from skimage.metrics import structural_similarity as simm
from tqdm import tqdm
#pylint: disable=redefined-builtin
from xlsxwriter import Workbook, format, worksheet
from utils.utils import get_paired_samples



@attr.s(auto_attribs=True)
class SampleEvalStats:
    """Wraps sample specific evaluations stats

    Args:
        sample_name: Name of the generated sample
        simm_stats: Structural similarity score stats
        psnr_stats: Peak signal to noise ratio score stats
        prcr_stats: Pearson correlation coefficient stats
    """
    sample_name: str
    simm_stats: Mapping[str, float]
    psnr_stats: Mapping[str, float]
    prcr_stats: Mapping[str, float]

class Evaluate:
    """Evaluate WSI using SIMM, PSNR, & PRCR"""

    def __init__(
            self,
            config: Mapping[str, Any],
            exp_path: str,
            multiprocess: bool,
            inference_root: Optional[str] = None,
            data_root: Optional[str] = None
        ) -> None:
        self._tile_format = config['tile_format']
        self._exp_path = exp_path
        self._multiprocess = multiprocess
        self._inference_root = inference_root
        self._data_root = data_root
        self._wsi_pair_list = get_paired_samples(
            config['generated'],
            config['ground_truth']
        )
        self._img_epoch_pairs = list(product(
           self._wsi_pair_list, config['epochs']))
        self._write_tilewise_stats = config.get('write_tilewise_stats', False)

    def __call__(self) -> None:
        """Compute SIMM, PSNR, PRCR based evalation and saves results in a excel"""
        start_time = perf_counter()
        if self._multiprocess:
            pool = Pool(10)
            results = pool.map(self._evaluate, self._img_epoch_pairs)
        else:
            results: List[Tuple[int, str, Mapping]] = []
            for img_epoch_pair in tqdm(self._img_epoch_pairs):
                results.append(self._evaluate(img_epoch_pair))

        epoch_stats = self._dictify_evaluation_results(results=results)

        EvaluationWriter(
            exp_path=self._exp_path).generated_stats_sheet(
                epoch_stats=epoch_stats
            )
        elapsed = (perf_counter() - start_time)//60
        print(f'Evaluation took {elapsed} minutes')

    def _evaluate(self, img_epoch_pair: Sequence[Tuple[Tuple[int, str], str]]):
        """Run evaluation on generated and ground truth WSI pair tile by tile"""
        img_pair, epoch = img_epoch_pair
        generated_wsi, ground_truth_wsi = img_pair
        generated_tiles, ground_truth_tiles = self._get_tile_pairs(
            generated_wsi=generated_wsi,
            ground_truth_wsi=ground_truth_wsi,
            epoch=epoch)
        simm_scores, psnr_scores, prcr_scores = self._get_evaluation_metric_scores(
            generated_tiles=generated_tiles, ground_truth_tiles=ground_truth_tiles
        )
        simm_stats = self._get_stats(arr=array(list(simm_scores.values())))
        psnr_stats = self._get_stats(arr=array(list(psnr_scores.values())))
        prcr_stats = self._get_stats(arr=array(list(prcr_scores.values())))
        stats_dict = {'simm': simm_stats, 'psnr': psnr_stats, 'prcr': prcr_stats}

        if self._write_tilewise_stats:
            df_simm =  pd.DataFrame(simm_scores.items(), columns=['tile', 'simm'])
            df_psnr =  pd.DataFrame(psnr_scores.items(), columns=['tile', 'psnr'])
            df_prcr =  pd.DataFrame(prcr_scores.items(), columns=['tile', 'prcr'])

            df_merged = pd.merge(pd.merge(df_psnr, df_prcr, on='tile'), df_simm, on='tile')
            pkl_path = join(
                self._exp_path,
                'evaluation',
                f'{ground_truth_wsi.split(".")[0]}_epoch_{epoch}.pkl'
            )
            df_merged.to_pickle(pkl_path)

        return epoch, generated_wsi, stats_dict

    def _get_tile_pairs(
            self,
            generated_wsi: str,
            ground_truth_wsi: str,
            epoch: int
        ) -> Tuple[List[str], List[str]]:
        """Return list of tiles for generated and stained ground truth WSIs"""
        generated_sample_name = generated_wsi.split('.')[0]
        ground_truth_sample_name = ground_truth_wsi.split('.')[0]
        generated_tiles_path =  join(
                self._inference_root, f'epoch{epoch:03d}', generated_sample_name
            ) if self._inference_root else join(
                self._exp_path, 'inference', f'epoch{epoch:03d}', generated_sample_name)
        ground_truth_tiles_path = join(self._data_root, 'stained', ground_truth_sample_name)
        generated_tiles = [f for f in listdir(generated_tiles_path) if self._tile_format in f]
        ground_truth_tiles = [f for f in listdir(ground_truth_tiles_path) if self._tile_format in f]
        generated_tiles.sort()
        ground_truth_tiles.sort()
        if len(generated_tiles) != len(ground_truth_tiles):
            raise RuntimeError(f'Unequal tiles in {generated_sample_name}'
                               f' and {ground_truth_sample_name}')
        #return full paths
        return ([join(generated_tiles_path, f) for f in generated_tiles],
               [join(ground_truth_tiles_path, f) for f in ground_truth_tiles])

    def _get_evaluation_metric_scores(
            self,
            generated_tiles: Sequence[str],
            ground_truth_tiles: Sequence[str]
        ) -> Tuple[dict, dict, dict]:
        """Return a list of tile wise simm, psnr, prcr scores"""
        simm_scores: dict = {}
        psnr_scores: dict = {}
        prcr_scores: dict = {}
        for gen_tile_path, gt_tile_path in zip(generated_tiles, ground_truth_tiles):
            gen_tile = array(Image.open(gen_tile_path))
            gt_tile = array(Image.open(gt_tile_path))
            tile_name = basename(gen_tile_path).split('.')[0]
            simm_scores[tile_name] = \
                compute_simm(ground_truth_tile=gt_tile, generated_tile=gen_tile)
            psnr_scores[tile_name] = \
                compute_psnr(ground_truth_tile=gt_tile, generated_tile=gen_tile)
            prcr_scores[tile_name] = \
                compute_pearson_r(ground_truth_tile=gt_tile, generated_tile=gen_tile)
        return simm_scores, psnr_scores, prcr_scores

    @staticmethod
    def _get_stats(arr: ndarray) -> Mapping[str, float]:
        """Return min, max, mean, 95th percenile"""
        return {
            'mean' : np_mean(arr),
            'min' : np_min(arr),
            'max' : np_max(arr),
            'q95' : percentile(arr, 95)
        }

    @staticmethod
    def _dictify_evaluation_results(
            results: Sequence[Tuple[int, str, Mapping]]
        ) -> Dict[int, List[SampleEvalStats]]:
        """Unwrap (evaluation) list of Tuple and store them in a dictionary"""
        epoch_stats = defaultdict(list)
        for stat in results:
            epoch_stats[stat[0]].append(
                SampleEvalStats(
                    sample_name=stat[1],
                    simm_stats=stat[2]['simm'],
                    psnr_stats=stat[2]['psnr'],
                    prcr_stats=stat[2]['prcr']
                )
            )
        return epoch_stats


class EvaluationWriter:
    """Write evaluation results to excel file for a specific experiment"""

    def __init__(self, exp_path: str, name : str='evaluation') -> None:
        self._evaluation_path = join(exp_path, 'evaluation')
        self._name = name

    def generated_stats_sheet(self, epoch_stats: Dict[int, List[SampleEvalStats]]):
        """Write sample evaulation stats to an excel sheet"""
        with Workbook(join(self._evaluation_path, f'{self._name}.xlsx')) as workbook:
            bold = workbook.add_format({'bold': 1})
            for key in epoch_stats:
                work_sheet = workbook.add_worksheet(name=f'Epoch {key}')
                sorted_sample_stats_list = sorted(epoch_stats[key], key=lambda x: x.sample_name)
                sample_count = len(sorted_sample_stats_list)
                self._write_headers(work_sheet=work_sheet, frmt=bold, sample_count=sample_count)
                simm_mean: List[float] = []
                psnr_mean: List[float] = []
                prcr_mean: List[float] = []
                for i, sample_stats in enumerate(sorted_sample_stats_list):
                    self._write_stats(
                        work_sheet=work_sheet,
                        sample_stats=sample_stats,
                        row=i,
                        frmt=bold
                    )
                    simm_mean.append(sample_stats.simm_stats['mean'])
                    psnr_mean.append(sample_stats.psnr_stats['mean'])
                    prcr_mean.append(sample_stats.prcr_stats['mean'])
                self._write_summary_stats(work_sheet, simm_mean, sample_count, 1)
                self._write_summary_stats(work_sheet, psnr_mean, sample_count, 5)
                self._write_summary_stats(work_sheet, prcr_mean, sample_count, 9)

    @staticmethod
    def _write_headers(work_sheet: worksheet, frmt: format, sample_count: int):
        work_sheet.write(0, 0, 'Sample Name', frmt)
        work_sheet.write(0, 1, 'SIMM Mean', frmt)
        work_sheet.write(0, 2, 'SIMM Min', frmt)
        work_sheet.write(0, 3, 'SIMM Max', frmt)
        work_sheet.write(0, 4, 'SIMM Q95', frmt)
        work_sheet.write(0, 5, 'PSNR Mean', frmt)
        work_sheet.write(0, 6, 'PSNR Min', frmt)
        work_sheet.write(0, 7, 'PSNR Max', frmt)
        work_sheet.write(0, 8, 'PSNR Q95', frmt)
        work_sheet.write(0, 9, 'PRCR Mean', frmt)
        work_sheet.write(0, 10, 'PRCR Min', frmt)
        work_sheet.write(0, 11, 'PRCR Max', frmt)
        work_sheet.write(0, 12, 'PRCR Q95', frmt)
        work_sheet.write(sample_count + 2, 0, 'Mean', frmt)
        work_sheet.write(sample_count + 3, 0, 'Min', frmt)
        work_sheet.write(sample_count + 4, 0, 'Max', frmt)

    @staticmethod
    def _write_stats(
            work_sheet: worksheet,
            sample_stats: SampleEvalStats,
            row: int,
            frmt: format
        ):
        work_sheet.write(row + 1, 0, sample_stats.sample_name, frmt)
        work_sheet.write(row + 1, 1, sample_stats.simm_stats['mean'])
        work_sheet.write(row + 1, 2, sample_stats.simm_stats['min'])
        work_sheet.write(row + 1, 3, sample_stats.simm_stats['max'])
        work_sheet.write(row + 1, 4, sample_stats.simm_stats['q95'])
        work_sheet.write(row + 1, 5, sample_stats.psnr_stats['mean'])
        work_sheet.write(row + 1, 6, sample_stats.psnr_stats['min'])
        work_sheet.write(row + 1, 7, sample_stats.psnr_stats['max'])
        work_sheet.write(row + 1, 8, sample_stats.psnr_stats['q95'])
        work_sheet.write(row + 1, 9, sample_stats.prcr_stats['mean'])
        work_sheet.write(row + 1, 10, sample_stats.prcr_stats['min'])
        work_sheet.write(row + 1, 11, sample_stats.prcr_stats['max'])
        work_sheet.write(row + 1, 12, sample_stats.prcr_stats['q95'])

    @staticmethod
    def _write_summary_stats(
            work_sheet: worksheet,
            stat_mean: Sequence[float],
            sample_count: int,
            col: int
        ):
        work_sheet.write(sample_count + 2, col, np_mean(stat_mean))
        work_sheet.write(sample_count + 3, col, np_min(stat_mean))
        work_sheet.write(sample_count + 4, col, np_max(stat_mean))


def compute_simm(ground_truth_tile: ndarray, generated_tile: ndarray) -> float:
    """Compute structural similarity index measure between ground truth and generated tile"""
    gt_tile = rgb2gray(ground_truth_tile)
    gen_tile = rgb2gray(generated_tile)
    data_range = np_max(generated_tile) - np_min(generated_tile)
    msimm, _, _ = simm(gt_tile, gen_tile, data_range=data_range, gradient=True, full=True)
    return msimm

def compute_psnr(ground_truth_tile: ndarray, generated_tile: ndarray) -> float:
    """Compute peak signal to noise ratio between ground truth and generated tile"""
    data_range = np_max(generated_tile) - np_min(generated_tile)
    return psnr(ground_truth_tile, generated_tile, data_range=data_range)

def compute_pearson_r(ground_truth_tile: ndarray, generated_tile: ndarray) -> float:
    """Compute Pearson correlation coefficient for ground truth and generated tile"""
    r_coef, _ = pearsonr(ground_truth_tile.flatten(), generated_tile.flatten())
    return r_coef

def rgb2gray(rgb: ndarray):
    """RGB to grayscale"""
    return dot(rgb[...,:3], [0.2989, 0.5870, 0.1140])
