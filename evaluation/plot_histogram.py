"""Implement histogram plotting for different evaluation metric"""

from argparse import ArgumentParser
from collections import defaultdict
from os import listdir
from os.path import join

import pandas as pd
from matplotlib import pyplot as plt
from tqdm import tqdm


def plot_histogram(
        pickled_df_path: str
    ):
    """Plot histograms by reading pickled dataframes"""
    pkl_list = [pkl for pkl in listdir(pickled_df_path) if '.pkl' in pkl]
    sample_names = [f'sample_{pkl.split("_")[1]}' for pkl in pkl_list]

    eval_dict = defaultdict(list)
    # Sample-wise evaluation
    for pkl, sample_name in tqdm(zip(pkl_list, sample_names)):
        pkl_path = join(pickled_df_path, pkl)
        sample_df = pd.read_pickle(pkl_path)
        for eval_metric in ['simm', 'psnr', 'prcr']:
            arr = list(sample_df[eval_metric])
            eval_dict[eval_metric].extend(arr)
            plt.hist(arr, density=False, bins=100, fc='green', ec='yellow', alpha=0.75)
            plt.ylabel('Tiles')
            plt.xlabel(f'{eval_metric} Score')
            plt.grid(True)
            plt.savefig(join(pickled_df_path, f'{eval_metric}_{sample_name}.png'))
            plt.close()

    # Overall evaluation
    for eval_metric in ['simm', 'psnr', 'prcr']:
        plt.hist(
            eval_dict[eval_metric], density=False, fc='green', ec='yellow',
            bins=100, facecolor='g', alpha=0.75)
        plt.ylabel('Tiles')
        plt.xlabel(f'{eval_metric} Score')
        plt.grid(True)
        plt.savefig(join(pickled_df_path, f'{eval_metric}_overall.png'))
        plt.close()

def main():
    """Takes aguements and initiate histogram plotting"""

    parser = ArgumentParser()
    parser.add_argument(
        '--pkl_path', help='Contains tile stats pickles', type=str, required=True)

    param = parser.parse_args()
    plot_histogram(pickled_df_path=param.pkl_path)

if __name__ == '__main__':
    main()
