"""Evaluate from pickle using mask"""

from argparse import ArgumentParser
from collections import defaultdict
from os import listdir
from os.path import join
from typing import Dict, List, Mapping, Optional, Sequence, Tuple

import numpy as np
import pandas as pd
from pytiff import Tiff
from tqdm import tqdm

from .evaluate import EvaluationWriter, SampleEvalStats


def evaluate_from_pickle(
        exp_path: str,
        wsi_root: Optional[str] = None
    ):
    """Generate evaluation excel from pickle using masks"""
    eval_path = join(exp_path, 'evaluation')
    pkl_list = [pkl for pkl in listdir(eval_path) if '.pkl' in pkl]
    results = []
    for pkl in tqdm(pkl_list):
        df_pkl = pd.read_pickle(join(eval_path, pkl))
        pkl_parts = pkl.split('_')
        sample_name = f'sample_{pkl_parts[1]}'
        print(f'{sample_name} total overall tiles : {len(df_pkl)}')
        full_tissue = [False for _ in range(len(df_pkl))]
        df_pkl['full_tissue'] = full_tissue
        epoch = pkl_parts[4].split('.')[0]
        mask_path = join(wsi_root, sample_name, f'{sample_name}_mask.tif')
        with Tiff(mask_path) as tif_mask:
            mask = np.array(tif_mask).astype('bool')
        valid_tiles = tile_generator(
            mask=mask,
            tile_size=512,
            stride=512,
            accpetable_pc=100
        )
        simm_scores = []
        psnr_scores = []
        prcr_scores = []
        for row, col in valid_tiles:
            tile = f'{sample_name}_unstained_{row}_{col}'
            df_pkl.loc[df_pkl['tile'] == tile, "full_tissue"] = True
            tile_scores = df_pkl.loc[df_pkl['tile'] == tile]
            simm_scores.append(list(tile_scores['simm'])[0])
            psnr_scores.append(list(tile_scores['psnr'])[0])
            prcr_scores.append(list(tile_scores['prcr'])[0])
        df_pkl.to_pickle(join(eval_path, pkl))
        print(f'{sample_name} total tissue tiles {len(simm_scores)}')
        simm_stats = get_stats(arr=simm_scores)
        psnr_stats = get_stats(arr=psnr_scores)
        prcr_stats = get_stats(arr=prcr_scores)
        stats_dict = {'simm': simm_stats, 'psnr': psnr_stats, 'prcr': prcr_stats}
        results.append((epoch, f'{sample_name}_unstained.tif', stats_dict))

    epoch_stats = dictify_evaluation_results(results=results)

    EvaluationWriter(
        exp_path=exp_path,
        name='evaluation_100pc').generated_stats_sheet(
                epoch_stats=epoch_stats
            )

def dictify_evaluation_results(
        results: Sequence[Tuple[int, str, Mapping]]
    ) -> Dict[int, List[SampleEvalStats]]:
    """Unwrap (evaluation) list of Tuple and store them in a dictionary"""
    epoch_stats = defaultdict(list)
    for stat in results:
        epoch_stats[stat[0]].append(
            SampleEvalStats(
                sample_name=stat[1],
                simm_stats=stat[2]['simm'],
                psnr_stats=stat[2]['psnr'],
                prcr_stats=stat[2]['prcr']
            )
        )
    return epoch_stats

def get_stats(arr: np.ndarray) -> Mapping[str, float]:
    """Return min, max, mean, 95th percenile"""
    return {
        'mean' : np.mean(arr),
        'min' : np.min(arr),
        'max' : np.max(arr),
        'q95' : np.percentile(arr, 95)
    }

def tile_generator(
        mask: np.ndarray,
        tile_size: int,
        stride: int,
        accpetable_pc: float
    ):
    """A generator that returns tiles from tiff in a grid-like way"""
    img_h, img_w = mask.shape[0:2]
    rows = np.floor((img_h - tile_size + stride) / stride).astype("int")
    cols = np.floor((img_w - tile_size + stride) / stride).astype("int")
    for row_ind in range(rows):
        for col_ind in range(cols):
            # Top-left coordinates of tile at full-res.
            h_start = row_ind * stride
            w_start = col_ind * stride
            # Bottom-right coordinates of tile at full-res.
            mask_tile = \
                mask[h_start : h_start + tile_size, w_start : w_start + tile_size]
            if np.sum(mask_tile) <\
                tile_size * tile_size * (accpetable_pc/100):
                continue
            yield row_ind, col_ind

def main():
    """The main function"""

    parser = ArgumentParser()
    parser.add_argument(
        '--exp_path', help='Full path to the experiment', type=str, required=True)
    parser.add_argument(
        '--wsi_root', help='Full path to wsi masks', type=str, required=True)

    param = parser.parse_args()
    evaluate_from_pickle(exp_path=param.exp_path, wsi_root=param.wsi_root)

if __name__ == '__main__':
    main()
