# Introduction
The codebase was developed using python 3.7.3. All the dependencies can be installed using requirements.txt. There are five main steps, namely, tiling, training, inference, merge, and evaluation. All of these steps are configuration driven and the config files already contain almost all the settings except for some directories and epoch information. Instructions to set these variables are mentioned below in their respective sections. 
**Note**: The main repository folder will be referred to as root in the instructions below. 
# Tiling:
Tiling is the process of splitting a full-resolution tissue image into smaller patches for training and inference. Tiling configs for both train and test sets can be found in the folder root/configs/tiling. For the train set, use config_tiling_paired_train.json. Set "data_source" to the directory containing full-resolution tiff images and "output_root" to the tiles output director. To tile the full-resolution images use the following commands:

```
python execute.py tile --config root/configs/tiling/config_tiling_paired_train.json --multiprocess
```

```
python execute.py tile --config root/configs/tiling/config_tiling_paired_test.json --multiprocess
```

# Training:
Six training config files could be found in the folder root/configs/train, 2 per variant of pix2pix. Each training will generate an experiment folder like **Exp-Pix2Pix-02092021-124502** with the following folders and files:
**checkpoint**: contains epoch's model weights
**config.json**: config with which the training was started
**ds_wsi**: for downsampled whole slide images
**evaluation**: for evaluation file
**inference**: for epoch and sample-wise virtually stained tiles
**logs**: for training logs
**output**: for post-epoch validation sample visualization
**readme.txt**: contains git commit has
**wsi**: for full-resolution tissue whole slide images. 

Use the following python command to run the trainings with different training configs:
```
python execute.py train --exp_root "experiment-directory" --data_root "tiles-directory"/train/512/ --config root/configs/train/config_train_pix2pix_light.json
```
_experiments-directory_: where you want to save the experiments.
_tiles-directory_: where the tiles are saved. 
# Inference:
Similar to training, there are six inference configuration files in the folder root/configs/inference, 2 per variant of pix2pix. Add the epoch numbers, for which you want to run inference, to the "epochs" list in the configs. Inference will save epoch and sample-wise tiles under the inference folder in Use the following python command to run the inference with different inference configs:
```
python execute.py inference --exp_path "experiment-directory"/"experiment-name" --data_root "tiles-directory"/test/2048/ --config root/configs/inference/config_inference_pix2pix_light.json
```
_experiments-directory_: directory where experiments are saved.
_experiment-name_: Name of the specific experiment something like: Exp-Pix2Pix-06032022-201735
_tiles-directory_: where the tiles are saved.
# Merge:
The next step is merging the experiment specific tiles. Merge also has six config files in the folder root/configs/merge. Add the epoch numbers, for which you want to merge inference results, to the "epochs" list in the configs. Merge will generate ful.Use the following python command to run merge tiles with different merge configs:
```
python execute.py merge_tiles --exp_path "experiment-directory"/"experiment-name" --inference_root "experiment-directory"/"experiment-name"/inference --multiprocess --config ./configs/merge/config_merge_pix2pix_light.json
```
Additionally, you could also downsample the whole slide image by a factor of 10 and save them as jpegs. Use the following python command for that:
poetry run python execute.py downsample --exp_path "experiment-directory"/"experiment-name"

_experiments-directory_: directory where experiments are saved.
_experiment-name_: Name of the specific experiment something like: **Exp-Pix2Pix-06032022-201735**
# Evaluation:
The final step is tile-based evaluation. Evaluation produces an excel file containing tile-wise SSIM, PCC, and PSNR scores.
```
python execute.py evaluate --exp_path "experiment-directory"/"experiment-name" --data_root "tiles-directory"/test/2048  --inference_root "experiment-directory"/"experiment-name"/inference --multiprocess --config ./configs/evaluation/config_evaluate_pix2pix_light.json
```
_experiments-directory_: directory where experiments are saved.
_experiment-name_: Name of the specific experiment something like: **Exp-Pix2Pix-06032022-201735**
_tiles-directory_: where the tiles are saved.

# Project DOI
10.5281/zenodo.7589356

# How to cite this work
Khan, U., Koivukoski, S., Valkonen, M., Latonen, L., & Ruusuvuori, P. (2023). The effect of neural network architecture on virtual H&E staining: Systematic assessment of histological feasibility. _Patterns_
